import React from 'react';
import './App.css';

import Home from './components/Home.jsx';
import NewAd from './components/NewAd.jsx';
import Login from './components/Login.jsx';
import Register from './components/Register.jsx';
import Profile from "./components/Profile";

import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    Redirect
} from 'react-router-dom';
import Landing from "./components/Landing";

function isLogged()
{
    return localStorage.getItem('accessToken') !== undefined && localStorage.getItem('accessToken') !== null;
}

function App() {
    return (
        <Router>
            <header className={window.location.pathname === '/welcome' ? 'd-none': ''}>
                <nav className="navbar fixed-top navbar-expand-lg navbar-dark blue scrolling-navbar">
                    <a className="navbar-brand" href="/"><strong>Donner pour le COVID</strong></a>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav mr-auto">
                            <li className="nav-item">
                                <Link className="nav-link" to="/annonces">
                                    Annonces
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/deposer-une-annonce">
                                    Déposer une annonce
                                </Link>
                            </li>
                        </ul>
                        <ul className="navbar-nav nav-flex-icons">
                            <li className="nav-item">
                                <Link className="nav-link" to="/connexion">
                                    Connexion
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/inscription">
                                    Inscription
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/profile">
                                    Mon Compte
                                </Link>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <main>
                <div className="container-fluid">
                    <Switch>
                        <Route exact path="/annonces">
                            <Home/>
                        </Route>
                        <Route exact path="/deposer-une-annonce"
                               component={
                                   () => {
                                       if (isLogged())
                                       {
                                           return <NewAd/>
                                       }
                                       else
                                       {
                                           return <Redirect to="/connexion"/>
                                       }
                                   }
                               }>
                        </Route>
                        <Route exact path="/connexion">
                            <Login/>
                        </Route>
                        <Route exact path="/inscription">
                            <Register/>
                        </Route>
                        <Route exact path="/profile">
                            <Profile/>
                        </Route>
                        <Route exact="/welcome">
                            <Landing/>
                        </Route>
                    </Switch>
                </div>
            </main>
        </Router>
    );
}

export default App;
