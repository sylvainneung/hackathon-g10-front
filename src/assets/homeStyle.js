const filterAndCardsStyle = {
    height: "calc(100vh - 6rem - 42px)",
    paddingBottom: "15px",
    overflowY: 'auto'
};

const mapStyle = {
    height: "calc(100vh - 6rem - 42px)",
    paddingBottom: "15px"
};

export {
    filterAndCardsStyle,
    mapStyle
}
