import React from 'react';
import { withRouter } from 'react-router-dom';
import {apiConfig} from "../api/config";
import Modal from "react-bootstrap/Modal";

const axios= require('axios')

class NewAd extends React.Component
{

    constructor(props)
    {
        super(props);

        this.state = {
            titre: '',
            description: '',
            adresse: '',
            zipCode: '',
            dons: 'matériel', // default,
            modalShow: false,
            isLoading: false,
            disabled: false,
            disabledTitle: false,
            disabledAdresse: false,
            zipCodeDisabled: false,
        };

        this.onChangeTitre          = this.onChangeTitre.bind(this);
        this.onChangeDescription    = this.onChangeDescription.bind(this);
        this.onChangeAdresse        = this.onChangeAdresse.bind(this);
        this.onChangeZipCode        = this.onChangeZipCode.bind(this);
        this.onChangeDons           = this.onChangeDons.bind(this);
        this.handleSubmit           = this.handleSubmit.bind(this);
    }

    onChangeTitre(event) {
        if(event.target.value.trim().length > 50 || event.target.value.trim().length < 2) {
            this.setState({
                disabledTitle: true,
            })
        } else {
            this.setState({
                titre: event.target.value,
            });
            this.setState({
                disabledTitle: false
            })
        }

    }

    onChangeDescription(event) {
        if(event.target.value.trim().length > 500 || event.target.value.trim().length < 10) {
            this.setState({
                disabled: true,
            })
        } else {
            this.setState({
                description: event.target.value
            });
            this.setState({
                disabled: false
            })
        }

    }

    onChangeAdresse(event) {
        let test = event.target.value.trim().split(" ").length > 2;

        if(event.target.value.length === 0 || !test) {
            this.setState({
                adresse:'',
                disabledAdresse: true,
            });
        } else {
            this.setState({
                adresse: event.target.value,
                disabledAdresse: false,
            });
        }

    }

    onChangeZipCode(event) {


        if( event.target.value.trim().length !== 5) {
            this.setState({
                zipCode: '',
                zipCodeDisabled: true
            });
        } else {
            this.setState({
                zipCode: event.target.value,
                zipCodeDisabled:false
            });
        }

    }

    onChangeDons(event) {
        this.setState({
            dons: event.target.value
        });
    }

    componentDidMount() {

        const isAuthenticated = async () => {

            if (localStorage.getItem('accessToken') !== undefined && localStorage.getItem('accessToken') !== null) {
                const config = {
                    headers: {Authorization: `Bearer ${localStorage.getItem('accessToken')}`}
                };
                const {status} = await axios.get(`${apiConfig.host}/auth/me`, config)
                return status
            } else {
                return 401
            }
        };

        isAuthenticated()
            .then( (res) => {
                if(res !== 200) {
                    this.props.history.push(`/connexion`);
                }
            })
            .catch(e => {
                localStorage.removeItem("accessToken")
                this.setState({
                    modalErrorShow: true,
                    modalErrorMsg: "Vous avez été déconnecté."
                });
                this.props.history.push(`/connexion`);
            })
    }

    handleSubmit(event) {
        event.preventDefault();

        if(this.state.titre.trim().length > 0 && this.state.description.trim().length > 0 && this.state.adresse.trim().length > 0 && this.state.zipCode.trim().length > 0 && this.state.dons.trim().length>0) {
            this.setState({
                isLoading: true
            });
            axios.post(`${apiConfig.host}/annonce`, {
                titre: this.state.titre,
                description: this.state.description,
                adresse: this.state.adresse,
                dons: this.state.dons,
                zipCode: this.state.zipCode
            }, {
                headers: {
                    'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
                }
            })
                .then((response) => {
                    if (response.status === 200)
                    {
                        this.setState({
                            modalShow: false,
                            errorMessage: "",
                            isLoading: false
                        });

                        this.props.history.push(`/annonces`);
                    }
                    else
                    {
                        this.setState({
                            modalShow: true,
                            errorMessage: response.data.data.message,
                            isLoading: false
                        })
                    }
                })
                .catch(e => {
                    this.setState({
                        modalShow: true,
                        errorMessage: "Une erreur est survenue, veuillez réessayer.",
                        isLoading: false
                    }) })
        }


    }

    render()
    {
        return (  
            <div className="mt-4 pt-5">

                    <Modal show={this.state.modalErrorShow}>
                        <Modal.Header>Erreur</Modal.Header>
                        <Modal.Body>{this.state.modalErrorMsg}</Modal.Body>
                        <Modal.Footer>
                            <button className="btn btn-danger" onClick={ () => this.setState({modalErrorShow: false})}> Fermer</button>
                        </Modal.Footer>
                    </Modal>

                <Modal show={this.state.modalShow}>
                    <Modal.Header>Erreur</Modal.Header>
                    <Modal.Body>{this.state.errorMessage}</Modal.Body>
                    <Modal.Footer>
                        <button className="btn btn-danger" onClick={ () => this.setState({modalShow: false})}> Fermer</button>
                    </Modal.Footer>
                </Modal>

                <form className="text-center border border-light p-4" style={{width: '70%', marginRight: 'auto', marginLeft:'auto', marginTop: '50px'}} onSubmit={this.handleSubmit} noValidate>     
                    <p className="h4 mb-4">Déposer une annonce</p>      
                        <div>
                            <input 
                                type="text"
                                placeholder="Titre de votre annonce (entre 2 et 50 caractères)"
                                className={this.state.disabledTitle ? 'is-invalid md-textarea form-control mb-4' : 'md-textarea form-control mb-4'}
                                name="titre"
                                id="titre"
                                noValidate 
                                onChange={this.onChangeTitre}
                            disabled={this.state.isLoading}/>
                        </div>

                        <div>
                            <textarea 
                                type="text"
                                placeholder="Décrivez en quelques mots ce que vous êtes en mesure de donner (entre 10 et 500 caractères)"
                                className={this.state.disabled ? 'is-invalid md-textarea form-control mb-4' : 'md-textarea form-control mb-4'}
                                name="description"
                                id="description"
                                rows="5"
                                noValidate
                                disabled={this.state.isLoading}
                                onChange={this.onChangeDescription} />
                        </div>
                        <div>
                            <input 
                                type="text"
                                placeholder="Adresse, exemple : 3 allée des dons"
                                className={this.state.disabledAdresse ? 'is-invalid md-textarea form-control mb-4' : 'md-textarea form-control mb-4'}
                                name="addresse"
                                id="adresse"
                                noValidate
                                disabled={this.state.isLoading}
                                onChange={this.onChangeAdresse} />
                        </div>
                        <div>
                            <input 
                                type="text"
                                placeholder="Code Postal, ex : 75000"
                                className={this.state.zipCodeDisabled ? 'is-invalid md-textarea form-control mb-4' : 'md-textarea form-control mb-4'}
                                name="zipCode"
                                id="zipCode"
                                noValidate
                                disabled={this.state.isLoading}
                                onChange={this.onChangeZipCode} />
                        </div>
                        <div>
                            <select 
                            className="browser-default custom-select" 
                            name="dons" 
                            id="dons" 
                            value={this.state.dons}
                            disabled={this.state.isLoading}
                            onChange={this.onChangeDons}>
                                <option value="materiel">Matériel</option>
                                <option value="nourriture">Nourriture</option>
                                <option value="scolaire">Soutient scolaire</option>
                                <option value="temps">Temps libre</option>
                                <option value="déplacement">Déplacement</option>
                                <option value="autre">Autre</option>
                            </select>
                        </div>
                        <div>

                            <button className="btn btn-primary" disabled={this.state.isLoading || this.state.disabled}> Déposer
                                <span className={this.state.isLoading ? 'ml-1 spinner-border spinner-border-sm': 'd-none'}></span>
                            </button>

                        </div>                            
                </form>
            </div>                                                             
        );
    }
}

export default withRouter(NewAd);
