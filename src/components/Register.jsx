import React from 'react';

import axios from 'axios';
import {withRouter} from "react-router-dom";
import {apiConfig} from "../api/config";
import Modal from "react-bootstrap/Modal";

class Register extends React.Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            firstName: '',
            lastName: '',
            email: '',
            password: '',
            phoneNumber: '',
            modalShow: false
        };

        this.onChangeFirstName      = this.onChangeFirstName.bind(this);
        this.onChangeLastName       = this.onChangeLastName.bind(this);
        this.onChangeEmail          = this.onChangeEmail.bind(this);
        this.onChangePassword       = this.onChangePassword.bind(this);
        this.onChangePhoneNumber    = this.onChangePhoneNumber.bind(this);
        this.onSubmit               = this.onSubmit.bind(this);
    }

    onChangeFirstName(event) {
        this.setState({
            firstName: event.target.value
        });
    }

    onChangeLastName(event) {
        this.setState({
            lastName: event.target.value
        });
    }

    onChangeEmail(event) {
        this.setState({
            email: event.target.value
        });
    }

    onChangePassword(event) {
        this.setState({
            password: event.target.value
        });
    }

    onChangePhoneNumber(event) {
        this.setState({
            phoneNumber: event.target.value
        });
    }

    async onSubmit(event) {
        event.preventDefault();

        axios.post(`${apiConfig.host}/auth/signin`, this.state, {})
            .then((response) => {
                if (response.status === 200)
                {
                    if (response.data.error !== undefined)
                    {
                        this.setState({
                            modalShow: true,
                            errorMessage: response.data.data,
                            isLoading: false
                        })
                    }
                    else
                    {
                        localStorage.setItem('accessToken', response.data.data);
                        this.props.history.push(`/annonces`);
                    }
                }
            })
            .catch(e => {
                this.setState({
                    modalShow: true,
                    errorMessage: e.message,
                    isLoading: false
                })
            })
    }

    render()
    {
        return (
            <div className="mt-4 pt-5">

                <Modal show={this.state.modalShow}>
                    <Modal.Header>Erreur</Modal.Header>
                    <Modal.Body>{this.state.errorMessage}</Modal.Body>
                    <Modal.Footer>
                        <button className="btn btn-danger" onClick={ () => this.setState({modalShow: false})}> Fermer</button>
                    </Modal.Footer>
                </Modal>

                <form className="text-center border border-light p-4" style={{width: '70%', marginRight: 'auto', marginLeft:'auto', marginTop: '50px'}} onSubmit={this.onSubmit}>
                    <p className="h4 mb-4">Inscription</p>
                        <div>
                            <input  type="text" 
                                    name="firstName "
                                    id="firstName "
                                    placeholder="Votre nom"
                                    className="form-control mb-4"
                                    required
                                    onChange={this.onChangeFirstName}/>
                        </div>
                        <div>
                            <input  type="text" 
                                    name="lastName"
                                    id="lastName"
                                    placeholder="Votre prénom" 
                                    className="form-control mb-4"
                                    required
                                    onChange={this.onChangeLastName}/>
                        </div>
                        <div>
                            <input  type="password" 
                                    name="password"
                                    id="password"
                                    placeholder="Votre mot de passe" 
                                    className="form-control mb-4"
                                    required
                                    onChange={this.onChangePassword}/>
                        </div>
                        <div>
                            <input  type="email" 
                                    name="email"
                                    id="email"
                                    placeholder="Votre email" 
                                    className="form-control mb-4"
                                    required
                                    onChange={this.onChangeEmail}/>
                        </div>
                        <div>
                            <input  type="number" 
                                    name="phoneNumber"
                                    id="phoneNumber"
                                    placeholder="Votre numéro de téléphone" 
                                    className="form-control mb-4"
                                    required
                                    onChange={this.onChangePhoneNumber}/>
                        </div>
                        <button className="btn btn-info btn-block mt-4" variant="primary" type="submit" style={{width: 'fit-content'}}>
                            S'inscrire
                        </button>
                </form>
            </div>
        );
    }
}

export default withRouter(Register);
