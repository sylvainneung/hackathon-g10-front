import React from 'react';
import {Link, withRouter} from "react-router-dom";

import '../assets/landing.css';


class Landing extends React.Component
{
    constructor(props)
    {
        super(props);

        this.state = {};
    }

    render()

    {
        return (
            <div className="card card-image"
                 style={{backgroundImage: 'url(https://cdn.hipwallpaper.com/i/33/62/E7XK5d.jpeg)' }}>
                <div className="text-white text-center rgba-stylish-strong py-3 px-4">
                    <div className="py-2">

                        <div className="container">
                        <h2 className="card-title h2 my-4 py-4">Donner pour la luttre contre le covid19</h2>
                        <p style={{marginTop:'-15px'}}>
                            Etudiants, nous avons décidé de mettre en place cette application pour permettre à tout le monde de participer à la lutte contre le covid19
                            <br></br>
                            Cette application est disponible pour une durée temporaire.
                        </p>
                        <h5 className="h4 blue-text mt-4"><i className="fas fa-banned"></i> Des équipements, denrées alimentaires, du temps ? </h5>
                            <button className="btn blue-gradient mt-4" onClick={ () => {this.props.history.push('/annonces')}}>
                                <i className="fas fa-map left"></i> Trouver ou déposer une annonce
                            </button>
                        </div>

                        <div className="container-fluid">
                            <h3 className="text-center mt-4 orange-text">Contributeurs</h3>
                            <div className="row text-center mb-2 mt-4">
                                <div className="col-md-4">
                                    <img className="rounded-circle" src="https://media-exp1.licdn.com/dms/image/C4D03AQGt3FlJcaJZyg/profile-displayphoto-shrink_200_200/0?e=1591228800&v=beta&t=DID45arYypV_xYzevF4NyuiV660lLPhYdQUN51gGYMI"/>
                                    <p className="mt-2">
                                        <a href="https://github.com/sylvainSUPINTERNET" target="_blank"><i className="fab fa-github-alt"></i> Github</a>
                                    </p>
                                    <p>
                                        <a href="https://www.linkedin.com/in/sylvain-joly-3a7152aa/" target="_blank"><i className="fab fa-linkedin"></i> LinkedIn</a>
                                    </p>
                                </div>
                                <div className="col-md-4">
                                    <img className="rounded-circle" src="/avatar.jpg"/>
                                    <p className="mt-2">
                                        <a href="https://github.com/QuentinDenisot" target="_blank"><i className="fab fa-github-alt"></i> Github</a>
                                    </p>
                                    <p>
                                        <a href="https://fr.linkedin.com/in/quentin-denisot-661689120" target="_blank"><i className="fab fa-linkedin"></i> LinkedIn</a>
                                    </p>
                                </div>
                                <div className="col-md-4">
                                    <img className="rounded-circle" src="/avatar.jpg"/>
                                    <p className="mt-2">
                                        <a href="https://github.com/kivabien" target="_blank"><i className="fab fa-github-alt"></i> Github</a>
                                    </p>
                                    <p>
                                        <a href="https://fr.linkedin.com/in/thomas-mallet-497998121" target="_blank"><i className="fab fa-linkedin"></i> LinkedIn</a>
                                    </p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        );
    }
}


export default withRouter(Landing);
