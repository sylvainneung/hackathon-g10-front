import React from 'react';

import {Marker, Popup, Tooltip} from 'react-leaflet';

class MapMarker extends React.Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            lat: props.lat,
            lng: props.lng,
            title: props.title,
            dons: props.dons
        };
    }

    render()
    {
        return (
            <Marker position={[this.state.lat, this.state.lng]}>
                <Popup>{this.props.title}
                </Popup>
                <Tooltip>{this.props.dons}</Tooltip>
            </Marker>
        );
    }
}

export default MapMarker;
