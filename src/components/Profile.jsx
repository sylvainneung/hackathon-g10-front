import React from 'react';
import {apiConfig} from "../api/config";
import Modal from "react-bootstrap/Modal";
import {withRouter} from "react-router-dom";
import Moment from 'react-moment';

const axios = require('axios')

class Profile extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            user:"",
            annonces: [],
            modalShow: false,
            isLoading: false,
            confirmTargetAnnounce: ""
        };

        this.disableAnnounce = this.disableAnnounce.bind(this);
        this.confirmDisableAnnounce = this.confirmDisableAnnounce.bind(this);
    }

    isAuthenticated = async () => {

        if (localStorage.getItem('accessToken') !== undefined && localStorage.getItem('accessToken') !== null) {
            const config = {
                headers: {Authorization: `Bearer ${localStorage.getItem('accessToken')}`}
            };
            return await axios.get(`${apiConfig.host}/auth/me`, config)
        } else {
            this.setState({
                isLogged: false
            });
            return 401
        }
    };

    componentDidMount() {
        // try to get info from token onlyif something in accessToken
        if(localStorage.getItem("accessToken")) {
            console.log(localStorage.getItem('accessToken'))
            this.isAuthenticated()
                .then( (res) => {
                    if(res.status === 200) {
                        this
                            .setState({
                                user: res.data.data.data,
                                isLogged: true
                            })

                        axios
                            .post(apiConfig.host+'/annonce/profile', {}, {
                                headers: {
                                    'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
                                }
                            })
                            .then( response => {
                                if(response.status === 200) {
                                    this.setState({
                                        annonces: response.data.data // array
                                    })
                                }
                            })
                            .catch(e => {
                                this.setState({
                                    modalErrorShow: true,
                                    modalErrorMsg: "Une erreur est survenue, veuillez réessayer"
                                });
                            })
                    } else {
                        this.props.history.push(`/connexion`);
                    }
                })
                // error (this error display if the token is invalid and still inside storage
                .catch(e => {
                    this.setState({
                        modalErrorShow: true,
                        modalErrorMsg: "Vous avez été déconnecté."
                    });
                    this.setState({
                        isLogged: false
                    });

                    localStorage.removeItem("accessToken")
                        this.props.history.push(`/connexion`);
                    }

                )
        } else {
            this.props.history.push(`/connexion`);
        }
    }

    disableAnnounce(announce){
        this.setState({
            modalShow: true,
            confirmTargetAnnounce : announce
        });
    }
    confirmDisableAnnounce(){
        this.setState({
            isLoading: true
        });

        const {id} = this.state.confirmTargetAnnounce; // annonceId

        axios.post(apiConfig.host+'/annonce/disable', {
            "announceId": id
        }, {
            headers: {
                'Authorization': 'Bearer '+localStorage.getItem('accessToken')
            }
        })
            .then( (res) => {
                if(res.status === 200) {
                    this.setState({
                        modalShow: false,
                        isLoading: false
                    });
                    window.location.reload();
                } else {
                    this.setState({
                        modalShow: false,
                        isLoading: false
                    })
                }
            })
            .catch(e => {
                this.setState({
                    modalShow: false,
                    applyLoading: false
                })
            });
    }

    render() {
        return (
            <div className="mt-4 pt-5">

                    <Modal show={this.state.modalErrorShow}>
                        <Modal.Header>Erreur</Modal.Header>
                        <Modal.Body>{this.state.modalErrorMsg}</Modal.Body>
                        <Modal.Footer>
                            <button className="btn btn-danger" onClick={ () => this.setState({modalErrorShow: false})}> Fermer</button>
                        </Modal.Footer>
                    </Modal>

                <Modal show={this.state.modalShow}>
                    <Modal.Header>Erreur</Modal.Header>
                    <Modal.Body>{this.state.errorMessage}</Modal.Body>
                    <Modal.Footer>
                        <button className="btn btn-danger" onClick={ () => this.setState({modalShow: false})}> Fermer</button>
                    </Modal.Footer>
                </Modal>

                <div className="mt-3">

                    <div className="row">

                        <div className="col-md-6 mb-2">
                            <div className="card">
                                <div className="card-body">
                                    <div className=" text-center">
                                        <h4 className="card-title">Informations personnels</h4>
                                        <img className="rounded-circle img" alt="avatar" src={`https://www.gravatar.com/avatar/${this.state.user.hashGravatar}?d=identicon&f=y`}/>
                                    </div>
                                    <div className="card-text">
                                        <button className="pull-right btn btn-danger btn-md" onClick={ () => {
                                            localStorage.removeItem('accessToken')
                                            this.props.history.push('/connexion')
                                        }}>Déconnexion</button>
                                        <p>Nom : {this.state.user.lastName}</p>
                                        <p>Prénom : {this.state.user.firstName} </p>
                                        <p><i className="fa fa-phone"></i> {this.state.user.phoneNumber}</p>
                                        <p><i className="fa fa-envelope"></i> {this.state.user.email}</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="col-md-6">

                            <div className={this.state.annonces.length === 0 ? '' : 'd-none'}>
                                <p className="alert alert-info text-center"> Vous n'avez pas posté d'annonces pour le moment. </p>
                            </div>

                            <div className={this.state.annonces.length > 0 ? '' : 'd-none'}>

                                {
                                    this.state.annonces.map((annonce) =>
                                        <div className="card">
                                            <div className="card-body">
                                                <span className="badge badge-danger ml-2 m-2">Plus actif</span>
                                                <h5 className="text-center">{annonce.titre}</h5>
                                                <p className="small">
                                                    <i className="fa fa-clock mr-1 text-light"></i>
                                                    <Moment format="YYYY/MM/DD - HH:MM:SS">
                                                        {annonce.createdAt}
                                                    </Moment>
                                                </p>

                                                <p className="card-text">{annonce.description}</p>

                                                <div className={annonce.active === true ? "text-center" : "d-none"}>
                                                    <button className="btn btn-primary" onClick={() => {this.disableAnnounce(annonce) } }>
                                                        Désactiver
                                                    </button>
                                                </div>

                                                <div className={annonce.active === false ? "text-center" : "d-none"}>
                                                    <button className="btn btn-primary" disabled={true}>
                                                        Désactiver
                                                    </button>
                                                </div>

                                                <Modal show={this.state.modalShow}>
                                                    <Modal.Header>Confirmation</Modal.Header>
                                                    <Modal.Body>Voulez-vous désactiver cette annonce ? (Celle-ci ne pourra plus faire de l'objet de nouvelle demande par les utilisateurs)</Modal.Body>
                                                    <Modal.Footer>
                                                        <button className="btn btn-primary" disabled={this.state.isLoading} onClick={this.confirmDisableAnnounce}> Accepter
                                                            <span className={this.state.isLoading ? 'ml-1 spinner-border spinner-border-sm': 'd-none'}></span>
                                                        </button>
                                                        <button className="btn btn-danger" onClick={ () => this.setState({modalShow: false})}> Refuser</button>
                                                    </Modal.Footer>

                                                </Modal>

                                            </div>
                                        </div>
                                    )}

                            </div>

                        </div>

                    </div>

                </div>
            </div>
        );
    }
}

export default withRouter(Profile);
