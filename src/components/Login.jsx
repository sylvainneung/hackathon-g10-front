import React from 'react';

import { withRouter } from 'react-router-dom';

import axios from 'axios';
import {apiConfig} from "../api/config";
import Modal from "react-bootstrap/Modal";

class Login extends React.Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            email: '',
            password: '',
            modalShow: false
        };

        this.onChangeEmail      = this.onChangeEmail.bind(this);
        this.onChangePassword   = this.onChangePassword.bind(this);
        this.onSubmit           = this.onSubmit.bind(this);
    }

    componentDidMount() {
    }

    onChangeEmail(event) {
        this.setState({
            email: event.target.value
        });
    }

    onChangePassword(event) {
        this.setState({
            password: event.target.value
        });
    }

    async onSubmit(event) {
        event.preventDefault();

        axios.post(`${apiConfig.host}/auth/login`, this.state, {})
            .then((response) => {
                if (response.status === 200)
                {
                    if (response.data.error !== undefined)
                    {
                        this.setState({
                            modalShow: true,
                            errorMessage: response.data.data,
                            isLoading: false
                        })
                    }
                    else
                    {
                        localStorage.setItem('accessToken', response.data.data);
                        this.props.history.push(`/annonces`);
                    }
                }
            })
            .catch(e => {
                this.setState({
                    modalShow: true,
                    errorMessage: e.message,
                    isLoading: false
                })
            })
    }

    render()
    {
        return (
            <div className="mt-4 pt-5">

                <Modal show={this.state.modalShow}>
                    <Modal.Header>Erreur</Modal.Header>
                    <Modal.Body>{this.state.errorMessage}</Modal.Body>
                    <Modal.Footer>
                        <button className="btn btn-danger" onClick={ () => this.setState({modalShow: false})}> Fermer</button>
                    </Modal.Footer>
                </Modal>

                <form className="text-center border border-light p-4" style={{width: '70%', marginRight: 'auto', marginLeft:'auto', marginTop: '50px'}} onSubmit={this.onSubmit}>
                    <p className="h4 mb-4">Connexion</p>
                        <div>
                            <input  type="email" 
                                    name="email"
                                    id="email"
                                    placeholder="Votre email"
                                    className="form-control mb-4"
                                    required
                                    onChange={this.onChangeEmail}
                            />
                        </div>
                        <div>
                            <input  type="password" 
                                    name="password"
                                    id="password"
                                    placeholder="Votre mot de passe" required
                                    className="form-control mb-4"
                                    onChange={this.onChangePassword}
                            />
                        </div>
                        <button className="btn btn-info btn-block" variant="primary" type="submit" style={{width: 'fit-content'}}>
                            Se connecter
                        </button>
                </form>
            </div>
        );
    }
}

export default withRouter(Login);
