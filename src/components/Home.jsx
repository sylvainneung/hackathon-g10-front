import React from 'react';

//import MapContainer from './MapContainer.jsx';

import {
    filterAndCardsStyle,
    mapStyle
} from '../assets/homeStyle.js';

import {apiConfig} from "../api/config";
import {Map, TileLayer} from "react-leaflet";
import MapMarker from "./MapMarker";
import ReactPaginate from 'react-paginate';

import Modal from "react-bootstrap/Modal";
import {Toast} from "react-bootstrap";
import Moment from 'react-moment'

const axios = require('axios');


class Home extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            location: {
                latitude: "",
                longitude: "",
            },
            locationTimestamp: "",
            zoom: 14,
            radius: 13,
            page: 0,
            dons: 'all',
            markers: [], // annonces
            markersTotal: "",
            itemPerPage: 6,
            user: "",
            isLogged: false,
            confirmedToast: false,
            confirmedToastMsg: "",
            confirmedErrorToast: false,
            confirmedErrorToastMsg: "",

            modalErrorShow: false,
            modalErrorMsg: "",
            isLoading: false,


            applyLoading: false
        };

        this.handleFilterChange = this.handleFilterChange.bind(this);
        this.confirmApply = this.confirmApply.bind(this);
        this.applyModal = this.applyModal.bind(this);
        this.closeToast = this.closeToast.bind(this);
        this.closeErrorToast = this.closeErrorToast.bind(this);
    }

    confirmApply() {
        this.setState({
            applyLoading: true
        });

        let userPhone = this.state.user.phoneNumber;
        let userEmail = this.state.user.email;

        const targetAnnounce = this.state.confirmTargetAnnounce

        axios.post(apiConfig.host+'/annonce/apply', {
            "announceId": targetAnnounce.id,
            "to": targetAnnounce.emailOwner,
            "titleDon": targetAnnounce.titre,
            "userPhone": userPhone,
            "userEmail": userEmail,
        }, {
            headers: {
                'Authorization': 'Bearer '+localStorage.getItem('accessToken')
            }
        })
            .then( (res) => {
                if(res.status === 200) {
                    this.setState({
                        confirmedToast: true,
                        confirmedToastMsg: res.data.data.message,
                        modalShow: false,
                        applyLoading: false
                    })
                    window.location.reload();
                } else {
                    this.setState({
                        confirmedErrorToast: true,
                        confirmedErrorToastMsg: res.data.data.message,
                        modalShow: false,
                        applyLoading: false
                    })
                }
            })
            .catch(e => {
                this.setState({
                    confirmedErrorToast: true,
                    confirmedErrorToastMsg: "Oups une erreur est survenue, veuillez réessayer.",
                    modalShow: false,
                    applyLoading: false
                })
            });

    }

    applyModal(announce){
        this.setState({
            modalShow: true,
            confirmTargetAnnounce : announce
        });
    }

    isAuthenticated = async () => {

        if (localStorage.getItem('accessToken') !== undefined && localStorage.getItem('accessToken') !== null) {
            const config = {
                headers: {Authorization: `Bearer ${localStorage.getItem('accessToken')}`}
            };
            return await axios.get(`${apiConfig.host}/auth/me`, config)
        } else {
            this.setState({
                isLogged: false
            });
            return 401
        }
    };

    componentDidMount() {

        // try to get info from token onlyif something in accessToken
        if(localStorage.getItem("accessToken")) {
            this.isAuthenticated()
                .then( (res) => {
                    if(res.status === 200) {
                        this
                            .setState({
                                user: res.data.data.data,
                                isLogged: true
                            })
                    }
                })
                // error (this error display if the token is invalid and still inside storage
                .catch(e => {
                    this.setState({
                        modalErrorShow: true,
                        modalErrorMsg: "Vous avez été déconnecté."
                    });
                    this.setState({
                        isLogged: false
                    });
                    localStorage.removeItem("accessToken")}
                    )
        }

        this.findCoordinates(); // init announce map with current position
        const leafletMap = this.leafletMap.leafletElement;
        leafletMap.on('zoomend', () => {
            this.syncAnnounceWithZoom(leafletMap.getZoom())
        });
    }

    syncAnnounceWithZoom(zoom) {
        this.setState({
            isLoading: true
        })
        const scales = {
            0: 4000,
            1: 3000,
            2: 2000,
            3: 1200,
            4: 900,
            5: 700,
            6: 400,
            7: 150,
            8: 100,
            9: 60,
            10: 50,
            11: 40,
            12: 30,
            13: 15,
            14: 13,
            15: 11,
            16: 10,
            17: 5,
            18: 2
        };

        const radius = scales[zoom];

        this.setState({
            zoom: zoom
        });

        axios
            .get(
                apiConfig.host + '/annonce?page=' + this.state.page + '&dons=' + this.state.dons + '&lat=' + this.state.location.latitude + '&lon=' + this.state.location.longitude + '&radius=' + radius, {
                })
            .then((res) => {
                if (res.status === 200) {
                    const {data} = res.data; // announces

                    // here dont need to check if data empty or not
                    this.setState({
                        markers: data,
                        markersTotal: res.data.size,

                        isLoading: false
                    })
                } else {
                    const {data} = res.data; // error
                    this.setState({
                        modalErrorShow: true,
                        modalErrorMsg: data.message,

                        isLoading: false
                    });
                }
            })
            .catch(e => this.setState({
                modalErrorShow: true,
                modalErrorMsg: "Une erreur est survenue, veuillez réessayer",

                isLoading: false
            }))

    }

    handlePageClick = (el) => {
        this.setState({
            isLoading: true
        });

        const p = el.selected;

        const scales = {
            0: 4000,
            1: 3000,
            2: 2000,
            3: 1200,
            4: 900,
            5: 700,
            6: 400,
            7: 150,
            8: 100,
            9: 60,
            10: 50,
            11: 40,
            12: 30,
            13: 15,
            14: 13,
            15: 11,
            16: 10,
            17: 5,
            18: 2
        };

        const radius = scales[this.state.zoom];

        axios
            .get(
                apiConfig.host + '/annonce?page=' + p + '&dons=' + this.state.dons + '&lat=' + this.state.location.latitude + '&lon=' + this.state.location.longitude + '&radius=' + radius, {})
            .then((res) => {
                if (res.status === 200) {
                    const {data} = res.data; // announces

                    // here dont need to check if data empty or not
                    this.setState({
                        markers: data,
                        markersTotal: res.data.size,

                        isLoading: false
                    })
                } else {
                    const {data} = res.data; // error
                    this.setState({
                        modalErrorShow: true,
                        modalErrorMsg: data.message,

                        isLoading: false
                    });
                }
            })
            .catch(e => {
                this.setState({
                    modalErrorShow: true,
                    modalErrorMsg: "Une erreur est survenue, veuillez réessayer",

                    isLoading: false
                });
            })

    };

    handleFilterChange(el){
        this.setState({
            isLoading: true
        })
        let filter = el.target.value;

        if(filter !== "nourriture" && filter !== "equipement" && filter !== "tous" && filter !== "scolaire" && filter !== "autre" && filter !== "déplacement" && filter !== "temps") {
            filter = "all"
        } else if(filter === "tous"){
            filter = "all"
        }


        this.setState({
            dons: filter
        }, () => {

            const scales = {
                0: 4000,
                1: 3000,
                2: 2000,
                3: 1200,
                4: 900,
                5: 700,
                6: 400,
                7: 150,
                8: 100,
                9: 60,
                10: 50,
                11: 40,
                12: 30,
                13: 15,
                14: 13,
                15: 11,
                16: 10,
                17: 5,
                18: 2
            };

            const radius = scales[this.state.zoom];

            axios
                .get(
                    apiConfig.host + '/annonce?page=' + 0 + '&dons=' + this.state.dons + '&lat=' + this.state.location.latitude + '&lon=' + this.state.location.longitude + '&radius=' + radius, {})
                .then((res) => {
                    if (res.status === 200) {
                        const {data} = res.data; // announces

                        // here dont need to check if data empty or not
                        this.setState({
                            markers: data,
                            markersTotal: res.data.size,

                            isLoading: false
                        })
                    } else {
                        const {data} = res.data; // error
                        this.setState({
                            modalErrorShow: true,
                            modalErrorMsg: data.message,

                            isLoading: false
                        });
                    }
                })
                .catch(e => this.setState({
                    modalErrorShow: true,
                    modalErrorMsg: "Une erreur est survenue, veuillez réessayer",

                    isLoading: false
                }))


        });
    };

    closeToast(){
        this.setState({
            confirmedToast: false
        })
    }

    closeErrorToast(){
        this.setState({
            confirmedErrorToast: false
        })
    }

    findCoordinates = () => {
        this.setState({
            isLoading: true
        });
        navigator.geolocation.getCurrentPosition(
            position => {
                this.setState({
                    location: position.coords,
                    locationTimestamp: position.timestamp
                });

                console.log(this.state.radius);
                axios
                    .get(
                        apiConfig.host + '/annonce?page=' + this.state.page + '&dons=' + this.state.dons + '&lat=' + this.state.location.latitude + '&lon=' + this.state.location.longitude + '&radius=' + this.state.radius, {
                        })
                    .then((res) => {
                        if (res.status === 200) {
                            const {data} = res.data; // announces
                            if (data.length === 0) {
                                this.setState({
                                    markers: data,
                                    markersTotal: res.data.size,
                                    isLoading: false

                                });
                            } else {
                                this.setState({
                                    markers: data,
                                    markersTotal: res.data.size,

                                    isLoading: false
                                })
                            }
                        } else {
                            const {data} = res.data; // error
                            this.setState({
                                modalErrorShow: true,
                                modalErrorMsg: data.message,

                                isLoading: false
                            })
                        }
                    })
                    .catch(e =>  this.setState({
                        modalErrorShow: true,
                        modalErrorMsg: "Une erreur est survenue. Veuillez réessayer",

                        isLoading: false
                    }))
            },
            error => this.setState({
                modalErrorShow: true,
                modalErrorMsg: "L'application nécessite votre géolocalisation pour fonctionner. Autoriser l'accès dans votre navigateur"
            }),
            {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000}
        );
    };

    displayList = () => {
        if (this.state.markers.length === 0 && this.state.isLoading === false) {
            return <p className="alert alert-warning text-center mt-4">Pas d'annonces dans ce secteur pour le moment</p>
        } else {
            return <div>
                {
                    this.state.markers.map((marker) =>
                        <div className="card m-2">
                            <div className="card-body">
                                <h5 className="text-center">{marker.titre}</h5>
                                <p className="small">
                                    <i className="fa fa-clock mr-1 text-light"></i>
                                    <Moment format="YYYY/MM/DD - HH:mm:ss">
                                        {marker.createdAt}
                                    </Moment>
                                </p>

                                <h4 className="card-title">{marker.title}</h4>
                                <p className="card-text">{marker.description}</p>

                                <div className="text-center">
                                <div className={this.state.isLogged === false ? '':'d-none'}>
                                    <a className="btn btn-sm btn-primary" href="/connexion">Connectez-vous pour faire une demande</a>
                                </div>
                                </div>
                                
                                <div className={this.state.isLogged === true ? '':'d-none'}>

                                    <div className={this.state.user.email === marker.emailOwner ? 'd-none': ''}>

                                            <div className={marker.applies !== null && marker.applies.find(e => e === this.state.user.id) ? '': 'd-none'}>
                                                <div className="alert alert-info text-center">Demande en attente</div>
                                            </div>
                                            <div className={marker.applies !== null && marker.applies.find(e => e === this.state.user.id) ? 'd-none': ''}>
                                                <div className="text-center">
                                                    <button className="btn btn-primary" onClick={() => {this.applyModal(marker) } }>
                                                        Demander le don
                                                    </button>
                                                </div>
                                            </div>

                                    </div>


                                </div>


                                <Modal show={this.state.modalShow}>
                                    <Modal.Header>Confirmation</Modal.Header>
                                    <Modal.Body>Confirmez-vous votre demande ? Si oui, un mail sera automatiquement envoyé, ainsi que vos coordonnées téléphonique</Modal.Body>
                                    <Modal.Footer>
                                        <button className="btn btn-primary" disabled={this.state.applyLoading} onClick={this.confirmApply}> Accepter
                                            <span className={this.state.applyLoading ? 'ml-1 spinner-border spinner-border-sm': 'd-none'}></span>
                                        </button>
                                        <button className="btn btn-danger" onClick={ () => this.setState({modalShow: false})}> Refuser</button>
                                    </Modal.Footer>

                                </Modal>

                            </div>
                        </div>
                    )}
            </div>
        }
    };

    render() {
        const position = [this.state.location.latitude, this.state.location.longitude];

        return (
            <div className="mt-4 pt-5">
                <Modal show={this.state.modalErrorShow}>
                    <Modal.Body>{this.state.modalErrorMsg}</Modal.Body>
                    <Modal.Footer>
                        <button className="btn btn-danger" onClick={ () => this.setState({modalErrorShow: false})}> Fermer</button>
                    </Modal.Footer>
                </Modal>

                <div className="custom-control custom-checkbox mt-3 mb-3 row">
                    <div className="col-md-3">
                        <i className="fa fa-filter mr-2 text-primary"></i>
                        <select className="browser-default custom-select" value={this.state.filter} onChange={this.handleFilterChange} style={{"width":"calc(100% - 24px)"}}>
                            <option value="tous">Tous</option>
                            <option value="materiel">Matériel</option>
                            <option value="nourriture">Nourriture</option>
                            <option value="scolaire">Soutient scolaire</option>
                            <option value="temps">Temps libre</option>
                            <option value="déplacement">Déplacement</option>
                            <option value="autre">Autre</option>
                        </select>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-6" style={filterAndCardsStyle}>

                        {this.displayList()}

                        <div className="text-center">
                            <span className={this.state.isLoading ? 'mt-5 spinner-border spinner-info spinner-border-lg': 'd-none'}></span>
                        </div>

                        <div className={this.state.markers.length > 0 ? 'mt-2' : 'd-none'}>
                            <ReactPaginate
                                previousLabel={'précédent'}
                                previousClassName={'page-item'}
                                previousLinkClassName={'page-link'}
                                nextLabel={'suivant'}
                                nextClassName={'page-item'}
                                nextLinkClassName={'page-link'}
                                breakLabel={'...'}
                                breakClassName={'page-item'}
                                breakLinkClassName={'page-link'}
                                pageCount={Math.ceil(this.state.markersTotal / this.state.itemPerPage)}
                                marginPagesDisplayed={this.state.markersTotal}
                                pageRangeDisplayed={5}
                                onPageChange={this.handlePageClick}
                                containerClassName={'pagination pg-blue mx-auto'}
                                pageLinkClassName={'page-link'}
                                pageClassName={'page-item'}
                                activeClassName={'active'}/>
                        </div>
                    </div>
                    <div className="col-md-6" style={mapStyle}>
                        <Map ref={m => {
                            this.leafletMap = m;
                        }}
                             center={position} zoom={this.state.zoom} style={{height: 'calc(100vh - 6rem - 42px)', borderRadius: '5px'}}>
                            <TileLayer
                                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                                attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
                            />

                            {this.state.markers.map((marker) =>
                                <MapMarker lat={marker.addrLat} lng={marker.addrLon} title={marker.titre}
                                           dons={marker.dons}/>
                            )}

                        </Map>
                    </div>
                </div>
            </div>
        );
    }
}

export default Home;
